from django.contrib import admin

from .models import Company, FriendCompany, InviteRequest, Notification, AffiliatedCompanyRequest


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', )

admin.site.register(Company, CompanyAdmin)


class FriendCompanyAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'status')
    search_fields = ['source_company__name', 'target_company__name']

admin.site.register(FriendCompany, FriendCompanyAdmin)


class InviteRequestAdmin(admin.ModelAdmin):
    list_display = ('sender', 'member_email', 'department', 'confirmed')

admin.site.register(InviteRequest, InviteRequestAdmin)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('user', 'source', 'target', 'dt_created')

admin.site.register(Notification, NotificationAdmin)


class AffiliatedCompanyRequestAdmin(admin.ModelAdmin):
    list_display = ('source_company', 'target_company', 'dt_created')

admin.site.register(AffiliatedCompanyRequest, AffiliatedCompanyRequestAdmin)
