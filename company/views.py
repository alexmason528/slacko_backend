import json
import datetime

from django.contrib.auth.models import User
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt
from rest_framework import filters, viewsets
from rest_framework.authentication import get_authorization_header
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.settings import api_settings
from constance import config

from .models import Company, FriendCompany, InviteRequest, Notification, AffiliatedCompanyRequest
from .models import COMPANY_DEPARTMENT_CHOICES_ARRAY
from .models import COMPANY_INDUSTRY_CHOICES_ARRAY
from .serializers import AffiliatedCompanySerializer, CompanyProfileSerializer
from .serializers import CompanySerializer, FriendCompanySerializer
from .serializers import NotificationSerializer
from userprofile.models import UserProfile
from userprofile.serializers import UserProfileSerializer

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class CompanyProfileUpdateView(RetrieveUpdateAPIView):
    """
    API view to get or update company profile
    """
    model = Company
    permission_classes = (IsAuthenticated,)
    serializer_class = CompanyProfileSerializer

    def get_object(self):
        return self.request.user.companies_created.first()


class CompanyProfileView(RetrieveUpdateAPIView):
    """
    API view to get or update company profile
    """
    model = Company
    permission_classes = (IsAuthenticated,)
    serializer_class = CompanySerializer

    def get_object(self):
        return self.request.user.companies_created.first()

    # def put(self, request, *args, **kwargs):
    #     body_unicode = request.body.decode('utf-8')
    #     # requestBody = json.loads(body_unicode)
    #     requestBody = request.data
    #     print(request.data)
    #     owner_data = requestBody['owner']
    #     affiliated_companies_data = requestBody['affiliated_companies']
    #     self.object = self.get_object()
    #     serializer = self.get_serializer(self.object, data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.object = serializer.save(force_update=True)
    #     self.post_save(self.object, created=False)
    #     return HttpResponse(serializer.data, status=200)


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for searching company with name or industry
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def get_queryset(self):
        companies = Company.objects.all()
        if self.request.GET.get('name', None):
            companies = companies.filter(
                name__icontains=self.request.GET['name'])
        if self.request.GET.get('industry', None):
            companies = companies.filter(
                industry__icontains=self.request.GET['industry'])
        return companies


class MemberViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing members for the company
    """
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    # def get_queryset(self):
    #     company = Company.objects.get(owner=self.request.user)
    #     return UserProfile.objects.filter(company=company)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('company', )


class FriendCompanyViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing members for the company
    """
    serializer_class = FriendCompanySerializer
    queryset = FriendCompany.objects.all()
    # def get_queryset(self):
    #     company = Company.objects.get(owner=self.request.user)
    #     return FriendCompany.objects.filter(source_company=company)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('source_company', )


class NotificationViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing members for the company
    """
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    permission_classes = (IsAuthenticated,)
    # def get_queryset(self):
    #     company = Company.objects.get(owner=self.request.user)
    #     return FriendCompany.objects.filter(source_company=company)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('user', 'source', 'target', )


class AffiliatedCompanyViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing members for the company
    """
    serializer_class = AffiliatedCompanySerializer

    def get_queryset(self):
        company = Company.objects.get(owner=self.request.user)
        return company.affiliated_companies


@csrf_exempt
def delete_friend_company(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    body_unicode = request.body.decode('utf-8')
    requestBody = json.loads(body_unicode)
    target = requestBody['target']
    source = Company.objects.get(owner=current_user)
    target_company = Company.objects.get(pk=target)
    source.friends.remove(target_company)

    description = '{} company deleted you from friend list'.format(source.name)
    notification = Notification.objects.create(
        user=target_company.owner,
        source=source,
        target=target_company,
        description=description,
    )
    notification.save()
    return JsonResponse({'status': 'Success', 'message': "Friend deleted!"})


@csrf_exempt
def invite_friend_company(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    body_unicode = request.body.decode('utf-8')
    requestBody = json.loads(body_unicode)
    target = requestBody['target']
    source = Company.objects.get(owner=current_user)
    target_company = Company.objects.get(pk=target)
    key = ''
    while True:
        key = get_random_string(64).lower()
        if not FriendCompany.objects.filter(key=key).exists():
            break
    friend_req = FriendCompany.objects.create(
        source_company=source, key=key, target_company=target_company)
    friend_req.send_email()
    friend_req.status = 1
    friend_req.save()
    return JsonResponse({'status': 'Success', 'message': "Invitation Email sent!"})


def list_department(request):
    department_list = list(COMPANY_DEPARTMENT_CHOICES_ARRAY)
    return JsonResponse(department_list, safe=False)


def list_industry(request):
    industry_list = list(COMPANY_INDUSTRY_CHOICES_ARRAY)
    return JsonResponse(industry_list, safe=False)


def list_friend_company(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    source_company = Company.objects.get(owner=current_user)
    friend_companies = FriendCompany.objects.filter(
        source_company=source_company)
    serializer = CompanySerializer(data=friend_companies)
    return JsonResponse(serializer.data, safe=False)


def list_affiliated_company(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    source_company = Company.objects.get(owner=current_user)
    affiliated_companies = source_company.affiliated_companies
    print(affiliated_companies)
    serializer = CompanySerializer(affiliated_companies)
    return JsonResponse(serializer.data)


@csrf_exempt
def delete_affiliated_company(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    body_unicode = request.body.decode('utf-8')
    requestBody = json.loads(body_unicode)
    target = requestBody['target']
    source = Company.objects.get(owner=current_user)
    target_company = Company.objects.get(pk=target)
    source.affiliated_companies.remove(target_company)

    description = '{} company deleted your company from the list affiliated companies'.format(source.name)
    notification = Notification.objects.create(
        user=target_company.owner,
        source=source,
        target=target_company,
        description=description,
    )
    notification.save()
    return JsonResponse({'status': 'Success', 'message': "Company deleted!"})


@csrf_exempt
def delete_member(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    body_unicode = request.body.decode('utf-8')
    requestBody = json.loads(body_unicode)
    company = Company.objects.get(owner=current_user)

    member_id = requestBody['user_id']
    member = UserProfile.objects.get(pk=member_id)

    if member.company == company:
        member_user = member.user
        member.delete()
        member_user.delete()

        return JsonResponse({'status': 'Success', 'message': "Member deleted!"})
    else:
        return JsonResponse({'status': 'Failed', 'message': "You cannot delete this member!"})


@csrf_exempt
def invite_affiliated(request):
    try:
        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        target = requestBody['target']
        target_company = Company.objects.get(pk=target)
        auth = get_authorization_header(request).split()
        payload = jwt_decode_handler(auth[1])
        current_user_id = payload.get('user_id')
        current_user = User.objects.get(pk=current_user_id)
        source = Company.objects.get(owner=current_user)

        new_invite = AffiliatedCompanyRequest.create(source_company=source, target_company=target_company)
        new_invite.clean()
        new_invite.send_email();

        return JsonResponse({'status': 'Success', 'message': "Invitation Email sent!"})
    except Exception as e:
        return JsonResponse({'status': 'Failed', 'message': 'Request cannot be send!'})


@csrf_exempt
def confirm_affiliated_company(request, key):
    if request.method == 'POST':
        try:
            affiliated_req = AffiliatedCompanyRequest.objects.get(key=key)
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'Failed', 'message': 'Request cannot be send!'})

        expiration_dt = affiliated_req.sent + datetime.timedelta(days=config.AFFILIATED_COMPANY_INVITE_EXPIRY)
        if expiration_dt <= timezone.now():
            return JsonResponse({'status': 'Failed', 'message': 'Request cannot be send!'})
        source_company = affiliated_req.source_company
        target_company = affiliated_req.target_company
        source_company.affiliated_companies.add(target_company)
        description = '{} company was added as affiliated.'.format(target_company.name)
        notification = Notification.objects.create(
            user=source_company.owner,
            source=target_company,
            target=source_company,
            description=description,
        )
        notification.save()
        return JsonResponse({'status': 'Success', 'message': "Company is invited"})
    else:
        return JsonResponse({'status': 'Failed', 'message': "Request method should be POST"})


@csrf_exempt
def confirm_friend(request, key):
    if request.method == 'POST':
        try:
            friend_req = FriendCompany.objects.get(key=key)
        except ObjectDoesNotExist:
            return JsonResponse({"status": "Failed", "message": "Your token has expired"})

        expiration_dt = friend_req.sent + datetime.timedelta(days=config.COMPANY_RESET_EXPIRY)
        if expiration_dt <= timezone.now():
            return JsonResponse({"status": "Failed", "message": "Your token has expired"})

        friend_req.status = 2
        source_company = friend_req.source_company
        target_company = friend_req.target_company
        source_company.friends.add(target_company)
        # source_company.affiliated_companies.add(target_company)
        # target_company.affiliated_companies.add(source_company)
        # source_company.save()
        # target_company.save()
        friend_req.save()
        description = '{} company accepted friend request'.format(
            target_company.name)
        notification = Notification.objects.create(
            user=source_company.owner,
            source=target_company,
            target=source_company,
            description=description,
        )
        notification.save()
        return JsonResponse({'status': 'Success', 'message': "Company is invited", 'source': source_company.id})
    else:
        return JsonResponse({'status': 'Failed', 'message': "Request method should be POST"})


@csrf_exempt
def invite_new_member(request):
    body_unicode = request.body.decode('utf-8')
    # print(request.build_absolute_uri('/api/v1/conversation_request/20/'))
    requestBody = json.loads(body_unicode)
    new_user_email = requestBody['email']
    department = requestBody['department']
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    sender = Company.objects.get(owner=current_user)
    confirmed = False
    key = ''
    while True:
        key = get_random_string(64).lower()
        if not InviteRequest.objects.filter(key=key).exists():
            break
    new_invite = InviteRequest.objects.create(
        sender=sender, key=key, member_email=new_user_email, department=department)
    user_results = User.objects.filter(email=new_user_email).count()
    if user_results > 0:
        return JsonResponse({'status': 'Failed', 'message': "Email already has been used."})
    new_user = User.objects.create(
        email=new_user_email, username=new_user_email)
    new_user.is_active = False
    new_user.save()
    new_invite.send_email()
    return JsonResponse({'status': 'Success', 'message': "Invitation Email sent!"})


def list_members(request):
    auth = get_authorization_header(request).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    company = Company.objects.get(owner=current_user)
    user_profiles = UserProfile.objects.filter(company=company)
    serializer = UserProfileSerializer(data=user_profiles)
    if serializer.is_valid():
        return JsonResponse(serializer.data, safe=False)
    return JsonResponse({"status": "Failed"})


@csrf_exempt
def confirm_invite(request, key):
    if request.method == 'POST':
        try:
            inviteRequest = InviteRequest.objects.get(key=key, confirmed=False)
        except ObjectDoesNotExist:
            return JsonResponse({"status": "Failed", "message": "Your token has expired"})

        expiration_dt = inviteRequest.sent + datetime.timedelta(days=config.USER_INVITE_EXPIRY)
        if expiration_dt <= timezone.now():
            return JsonResponse({"status": "Failed", "message": "Your token has expired"})

        email = inviteRequest.member_email
        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        first_name = requestBody['first_name']
        last_name = requestBody['last_name']
        password = requestBody['password']
        user = User.objects.get(email=email)
        user.is_active = True
        user.first_name = first_name
        user.last_name = last_name
        user.set_password(password)
        profile = UserProfile.objects.create(
            user=user, company=inviteRequest.sender, department=inviteRequest.department)
        user.save()
        profile.save()
        inviteRequest.delete()
        token = jwt_payload_handler(user)

        return JsonResponse({'status': 'Success',
                             'message': "You are now member of the {} company".format(inviteRequest.sender.name),
                             'email': email,
                             'company': inviteRequest.sender.name,
                             'token': jwt_encode_handler(token)})
    else:
        return JsonResponse({"status": "Failed", "message": "Request method should be POST"})
