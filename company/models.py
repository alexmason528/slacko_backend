from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _
from constance import config

from userprofile.helpers import EmailThread
from slackco_backend.utils import PathAndRename


class Company(models.Model):
    """
    This model represents Company
    """
    owner = models.ForeignKey(User, related_name="companies_created")

    # basic information
    name = models.CharField(max_length=200, unique=True)
    description = models.TextField(blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    logo = models.ImageField(
        upload_to=PathAndRename('company/logos/'), blank=True, null=True)
    cover = models.ImageField(
        upload_to=PathAndRename('company/covers/'), blank=True, null=True)

    friends = models.ManyToManyField('self', blank=True)
    industry = models.CharField(max_length=200, blank=True, null=True)
    company_type = models.CharField(max_length=200, blank=True, null=True)
    company_size = models.CharField(max_length=200, blank=True, null=True)
    founded = models.SmallIntegerField(blank=True, null=True)

    country = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)

    # social profiles
    facebook = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    instagram = models.URLField(blank=True, null=True)
    google_plus = models.URLField(blank=True, null=True)
    linkedin = models.URLField(blank=True, null=True)
    youtube = models.URLField(blank=True, null=True)
    github = models.URLField(blank=True, null=True)
    stackoverflow = models.URLField(blank=True, null=True)

    # affiliated companies
    affiliated_companies = models.ManyToManyField('self', blank=True)

    dt_created = models.DateTimeField(auto_now_add=True)
    dt_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Companies"

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# For MVP, company departments are fixed to bellow, but it would be
# changed later MVP
COMPANY_DEPARTMENT_CHOICES = (
    ('Management', 'Management'),
    ('PR Department', 'PR Department'),
    ('Dev Department', 'Dev Department'),
    ('UX Department', 'UX Department'),
    ('Law Department', 'Law Department'),
    ('Farmer VR Department', 'Farmer VR Department'),
    ('UI Department', 'UI Department'),
    ('Other', 'Other'),
)
COMPANY_DEPARTMENT_CHOICES_ARRAY = (
    ('Management'),
    ('PR Department'),
    ('Dev Department'),
    ('UX Department'),
    ('Law Department'),
    ('Farmer VR Department'),
    ('UI Department'),
    ('Other'),
)

# COMPANY_INDUSTRY_CHOICES = (
#     ('Science', 'Science'),
#     ('Technology', 'Technology'),
#     ('Internet', 'Internet'),
#     ('Software', 'Software'),
#     ('Sport', 'Sport'),
# )

COMPANY_INDUSTRY_CHOICES_ARRAY = [
    ('Science'),
    ('Technology'),
    ('Internet'),
    ('Software'),
    ('Sport'),
]


FRIEND_REQUEST_STATUS_CHOICES = (
    (0, 'Created'),
    (1, 'Sent'),
    (2, 'Approved'),
    (3, 'Cancelled'),
)


class FriendCompany(models.Model):
    """
    This model represents friend company
    """
    status = models.SmallIntegerField(
        choices=FRIEND_REQUEST_STATUS_CHOICES, default=0)
    source_company = models.ForeignKey(Company, related_name="friends_source")
    target_company = models.ForeignKey(Company, related_name="friends_target")
    key = models.CharField(max_length=64, default="key")
    sent = models.DateTimeField(null=True, blank=True)

    dt_created = models.DateTimeField(auto_now_add=True)
    dt_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Friend Request"
        verbose_name_plural = "Friend Requests"
        unique_together = (("source_company", "target_company"),)

    def __unicode__(self):
        return "%s vs %s" % (self.source_company.name, self.target_company.name)

    def clean(self):
        # validate if source and target company is same
        if self.source_company == self.target_company:
            raise ValidationError(
                _("Source and Target company can't be same."))

        # validate if those companies are friends already
        if FriendCompany.objects.filter(source_company=self.target_company, target_company=self.source_company).exists() or self.target_company.friends.filter(id=self.source_company.id).exists():
            raise ValidationError(
                _("Friend Request with these companies already exists."))

    @classmethod
    def create(cls, source_company, target_company):
        key = ''
        while True:
            key = get_random_string(64).lower()
            if not cls._default_manager.filter(key=key).exists():
                break

        status = 0
        return cls._default_manager.create(source_company=source_company, target_company=target_company, status=status, key=key)

    def send_email(self):
        """
        Send an activation email to user for confirmation
        """
        domain = config.FRONTEND_DOMAIN.rstrip('/')
        confirmation_link = domain + config.COMPANY_INVITE_LINK % self.key
        source = self.source_company.name
        sender = self.source_company.owner.email
        ctx_dict = {'confirmation_link': confirmation_link,
                    'source': source, 'sender': sender}
        message = render_to_string('email/friend_request_email.txt', ctx_dict)
        EmailThread("Slackco Friend Company Invitation", message,
                    settings.DEFAULT_FROM_EMAIL, [self.target_company.owner.email]).start()
        description = '{} company sent friend request'.format(source)
        notification = Notification.objects.create(
            user=self.target_company.owner,
            source=self.source_company,
            target=self.target_company,
            description=description,
            link=confirmation_link
        )
        notification.save()
        self.sent = timezone.now()
        self.save()


class InviteRequest(models.Model):
    """
    This model is used to verify user account with email address
    """
    sender = models.ForeignKey(Company, related_name="member_invite")
    sent = models.DateTimeField(null=True, blank=True)
    key = models.CharField(max_length=64, unique=True)
    member_email = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    confirmed = models.BooleanField(default=False)

    @classmethod
    def create(cls, sender, member_email):
        key = ''
        while True:
            key = get_random_string(64).lower()
            if not cls._default_manager.filter(key=key).exists():
                break
        return cls._default_manager.create(sender=sender, member_email=member_email, department=department, key=key)

    def send_email(self):
        """
        Send an activation email to user for confirmation
        """
        domain = config.FRONTEND_DOMAIN.rstrip('/')
        confirmation_link = domain + config.USER_INVITE_LINK % self.key
        source_company = self.sender.name
        ctx_dict = {'confirmation_link': confirmation_link,
                    'source_company': source_company}
        message = render_to_string('email/invitation_email.txt', ctx_dict)
        EmailThread("Slackco Member Invitation", message,
                    settings.DEFAULT_FROM_EMAIL, [self.member_email]).start()
        self.sent = timezone.now()
        self.save()


NOTIFICATION_STATUS_CHOICES = (
    (0, 'Unread'),
    (1, 'Read'),
)


class Notification(models.Model):
    """docstring for Notification"""
    user = models.ForeignKey(User, related_name="notifications_to_read")
    source = models.ForeignKey(Company, related_name="notifications_sent")
    target = models.ForeignKey(Company, related_name="notifications_reached")
    description = models.CharField(max_length=250, blank=True)
    link = models.URLField(blank=True, null=True)
    status = models.SmallIntegerField(
        choices=NOTIFICATION_STATUS_CHOICES, default=0)
    is_viewed = models.BooleanField(default=False)
    dt_created = models.DateTimeField(auto_now_add=True)
    dt_modified = models.DateTimeField(auto_now=True)

    class Meta(object):
        ordering = ('-dt_created', )


class AffiliatedCompanyRequest(models.Model):
    """
    This model represents affiliated companies requests
    """
    source_company = models.ForeignKey(Company, related_name="company_source")
    target_company = models.ForeignKey(Company, related_name="company_target")
    key = models.CharField(max_length=64, default="key")
    sent = models.DateTimeField(null=True, blank=True)

    dt_created = models.DateTimeField(auto_now_add=True)
    dt_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Affiliated Request"
        verbose_name_plural = "Affiliated Requests"
        unique_together = (("source_company", "target_company"),)

    def __unicode__(self):
        return "%s vs %s" % (self.source_company.name, self.target_company.name)

    def clean(self):
        # validate if source and target company is same
        if self.source_company == self.target_company:
            raise ValidationError(
                _("Source and Target company can't be same."))

        # validate if those companies are friends already
        if AffiliatedCompanyRequest.objects.filter(source_company=self.target_company, target_company=self.source_company).exists() or self.target_company.affiliated_companies.filter(id=self.source_company.id).exists():
            raise ValidationError(
                _("Affiliated Request with these companies already exists."))

    @classmethod
    def create(cls, source_company, target_company):
        key = ''
        while True:
            key = get_random_string(64).lower()
            if not cls._default_manager.filter(key=key).exists():
                break
        return cls._default_manager.create(source_company=source_company, target_company=target_company, key=key)

    def send_email(self):
        """
        Send an activation email to user for confirmation
        """
        domain = config.FRONTEND_DOMAIN.rstrip('/')
        confirmation_link = domain + config.AFFILIATED_COMPANY_INVITE_LINK % self.key
        source = self.source_company.name
        target = self.target_company.name
        ctx_dict = {'confirmation_link': confirmation_link, 'target': target,
                    'source': source}
        message = render_to_string('email/affiliated_company_request_email.txt', ctx_dict)
        EmailThread("Slackco Friend Company Invitation", message,
                    settings.DEFAULT_FROM_EMAIL, [self.target_company.owner.email]).start()
        # self.sent = timezone.now()
        description = '{} company want to add your {} company as affiliated.'.format(self.source_company.name, self.target_company.name)
        notification = Notification.objects.create(
            user=self.target_company.owner,
            source=self.source_company,
            target=self.target_company,
            description=description,
            link=confirmation_link
        )
        notification.save()
        self.sent = timezone.now()
        self.save()
