# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-25 17:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0009_friendcompany_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='logo',
            field=models.ImageField(blank=True, null=True, upload_to='company/logos/%Y/%m/%d'),
        ),
    ]
