# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-29 01:11
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('company', '0011_company_cover'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=250)),
                ('link', models.URLField(blank=True, null=True)),
                ('status', models.SmallIntegerField(choices=[(0, 'Unread'), (1, 'Read')], default=0)),
                ('dt_created', models.DateTimeField(auto_now_add=True)),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications_sent', to='company.Company')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications_reached', to='company.Company')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications_to_read', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
