from rest_framework import serializers

from .models import Company, FriendCompany, FRIEND_REQUEST_STATUS_CHOICES
from .models import InviteRequest, Notification
from userprofile.serializers import UserSerializer


class AffiliatedCompanySerializer(serializers.ModelSerializer):
    """
    Company serializer
    """
    class Meta:
        model = Company
        exclude = ('affiliated_companies', )


class CompanyProfileSerializer(serializers.ModelSerializer):
    """
    Company serializer
    """
    class Meta:
        model = Company
        exclude = ('owner', 'affiliated_companies', 'friends', )


class CompanyFriendSerializer(serializers.ModelSerializer):
    """
    Company serializer for friend field
    """
    class Meta:
        model = Company
        exclude = ('owner', 'affiliated_companies')


class FriendCompanyShortSerializer(serializers.ModelSerializer):
    """
    Company serializer
    """

    class Meta:
        model = FriendCompany
        exclude = ('source_company', 'key', 'dt_created', 'dt_modified', )


class CompanySerializer(serializers.ModelSerializer):
    """
    Company serializer
    """
    affiliated_companies = AffiliatedCompanySerializer(many=True)
    owner = UserSerializer(many=False)
    friends = CompanyFriendSerializer(many=True)
    friends_source = FriendCompanyShortSerializer(many=True)

    class Meta:
        model = Company
        # exclude = ('owner', 'affiliated_companies')


class FriendCompanySerializer(serializers.ModelSerializer):
    """
    Company serializer
    """
    target_company = CompanySerializer(many=False)

    class Meta:
        model = FriendCompany
        exclude = ('source_company', 'key', 'dt_created', 'dt_modified', )


class NotificationSerializer(serializers.ModelSerializer):
    """docstring for NotificationSerializer"""
    source = CompanyProfileSerializer(many=False)

    class Meta:
        model = Notification
