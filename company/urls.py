from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from .views import CompanyProfileView, CompanyProfileUpdateView
from .views import CompanyViewSet
from . import views

router = DefaultRouter()
router.register(r'list', CompanyViewSet)
urlpatterns = router.urls

urlpatterns += [
    # User profile api
    url(r'^profile/get/$', CompanyProfileView.as_view(), name='profile'),
    url(r'^profile/put/$', CompanyProfileUpdateView.as_view(), name='profile_update'),
    url(r'^friend/invite/$', views.invite_friend_company, name='invite_friend_company'),
    url(r'^friend/delete/$', views.delete_friend_company, name='delete_friend_company'),
    # url(r'^friend/list/$', views.list_friend_company, name='list_friend_company'),
    url(r'^affiliated/delete/$', views.delete_affiliated_company, name='delete_affiliated_company'),
    url(r'^affiliated/confirm/(?P<key>\w+)/$', views.confirm_affiliated_company, name='confirm_affiliated_company'),

    url(r'^affiliated/invite/$', views.invite_affiliated, name='invite_affiliated'),
    url(r'^affiliated/list/$', views.list_affiliated_company, name='list_affiliated_company'),
    url(r'^department/list/$', views.list_department, name='list_department'),
    url(r'^industry/list/$', views.list_industry, name='list_industry'),
    url(r'^friend/confirm/(?P<key>\w+)/$', views.confirm_friend, name='confirm_friend'),
    url(r'^member/invite/$', views.invite_new_member, name='invite_member'),
    url(r'^member/delete/$', views.delete_member, name='delete_member'),
    # url(r'^member/list/$', views.list_members, name='list_members'),
    url(r'^invite/confirm/(?P<key>\w+)/$', views.confirm_invite, name='confirm_invite'),
]
