import json
import logging

from channels import Group
from channels.sessions import channel_session
from django.contrib.auth.models import User
from django.core import serializers

from .models import Room, ChatFile
from .serializers import MessageSerializer

log = logging.getLogger(__name__)


@channel_session
def ws_connect(message):
    # Extract the room from the message. This expects message.path to be of the
    # form /chat/{label}/, and finds a Room if the message path is applicable,
    # and if the Room exists. Otherwise, bails (meaning this is a some othersort
    # of websocket). So, this is effectively a version of _get_object_or_404.
    log.debug("Enter weconnect")
    try:
        prefix, label = message['path'].decode('ascii').strip('/').split('/')
        if prefix != 'chat':
            log.debug('invalid ws path=%s', message['path'])
            return
        room = Room.objects.get(id=label)
    except ValueError:
        log.debug('invalid ws path=%s', message['path'])
        return
    except Room.DoesNotExist:
        log.debug('ws room does not exist label=%s', label)
        return

    log.debug('chat connect client=%s:%s',
              message['client'][0], message['client'][1])

    # Need to be explicit about the channel layer so that testability works
    # This may be a FIXME?
    Group('chat-' + label,
          channel_layer=message.channel_layer).add(message.reply_channel)
    message.channel_session['room'] = room.id


@channel_session
def ws_receive(message):
    # Look up the room from the channel session, bailing if it doesn't exist
    try:
        label = message.channel_session['room']
        room = Room.objects.get(id=label)
    except KeyError:
        log.debug('no room in channel_session')
        return
    except Room.DoesNotExist:
        log.debug('recieved message, buy room does not exist label=%s', label)
        return

    # Parse out a chat message from the content text, bailing if it doesn't
    # conform to the expected message format.
    try:
        data = json.loads(message['text'])
    except ValueError:
        log.debug("ws message isn't json text=%s", text)
        return

    if set(data.keys()) != set(('handle', 'type', 'message', 'files')):
        log.debug("ws message unexpected format data=%s", data)
        return

    if data:
        log.debug('chat message room=%s handle=%s type=%s message=%s files=%s',
                  room.id, data['handle'], data['type'], data['message'], data['files'])
        user_id = data['handle']
        files = data.pop('files')
        data['handle'] = User.objects.get(pk=user_id)
        m = room.messages.create(**data)
        for fid in files:
            try:
                ChatFile.objects.filter(id=fid).update(message=m)
            except:
                pass

        data = serializers.serialize('json', [m], ensure_ascii=False)
        data = data[1:-1]
        serializer = MessageSerializer(m)
        # See above for the note about Group
        Group('chat-' + str(label), channel_layer=message.channel_layer).send(
            {'text': json.dumps(serializer.data)})


@channel_session
def ws_disconnect(message):
    try:
        label = message.channel_session['room']
        room = Room.objects.get(id=label)
        Group('chat-' + str(label),
              channel_layer=message.channel_layer).discard(message.reply_channel)
    except (KeyError, Room.DoesNotExist):
        pass
