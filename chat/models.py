from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from constance import config

from slackco_backend.utils import PathAndRename
from userprofile.helpers import EmailThread
from company.models import Company, Notification
from userprofile.models import UserProfile



class Room(models.Model):
    started = models.ForeignKey(User, related_name='rooms_started')
    name = models.CharField(max_length=250)
    source = models.ForeignKey(Company, related_name='room_source')
    target = models.ForeignKey(Company, related_name='room_target')
    confirmed = models.BooleanField()
    key = models.CharField(max_length=250, blank=True)
    members = models.ManyToManyField(User, related_name='rooms', blank=True)

    def __unicode__(self):
        return self.name

MESSAGE_STATUS_CHOICES = (
    (0, 'Unread'),
    (1, 'Read'),
)


class Message(models.Model):
    room = models.ForeignKey(Room, related_name='messages')
    handle = models.ForeignKey(User, related_name='user_messages')
    type = models.CharField(max_length=10, default="text")
    message = models.TextField()
    status = models.SmallIntegerField(
        choices=MESSAGE_STATUS_CHOICES, default=0)
    timestamp = models.DateTimeField(default=timezone.now, db_index=True)

    def __unicode__(self):
        return '[{timestamp}] {handle}: {message}'.format(**self.as_dict())

    @property
    def formatted_timestamp(self):
        return self.timestamp.strftime('%b %-d %-I:%M %p')

    def as_dict(self):
        return {'handle': self.handle, 'type': self.type, 'message': self.message, 'timestamp': self.formatted_timestamp}

    class Meta(object):
        ordering = ('timestamp', )


class ChatFile(models.Model):
    """docstring for ChatFile"""
    file = models.FileField(upload_to=PathAndRename('chat/file/'), blank=True)
    filename = models.CharField(max_length=250, default="", blank=True)
    message = models.ForeignKey(Message, null=True, blank=True, related_name="files")


class ConversationDeleteLink(models.Model):
    """
    LoginMagicLink is used to login with magic link
    """
    chat = models.ForeignKey(Room, related_name="delete_links")
    user_profile = models.ForeignKey(UserProfile, related_name="chat_delete_links")
    source_company = models.ForeignKey(Company, related_name="chat_delete_source")
    sent = models.DateTimeField(null=True, blank=True)
    key = models.CharField(max_length=64, unique=True)

    @classmethod
    def create(cls, chat, source_company, user_profile):
        key = ''
        while True:
            key = get_random_string(64).lower()
            if not cls._default_manager.filter(key=key).exists():
                break
        return cls._default_manager.create(chat=chat,
                                           source_company=source_company,
                                           user_profile=user_profile,
                                           key=key)

    def send_email(self):
        """
        Send email with magic link to user
        """
        domain = config.FRONTEND_DOMAIN.rstrip('/')
        magic_link = domain + config.CHAT_DELETE_LINK % self.key
        ctx_dict = {
            'chat': self.chat,
            'delete_link': magic_link,
            'source_company': self.source_company,
        }
        message = render_to_string('email/chat_delete_email.txt', ctx_dict)
        EmailThread("Slackco Chat delete request", message, settings.DEFAULT_FROM_EMAIL, [
                    self.user_profile.user.email]).start()
        self.sent = timezone.now()
        self.save()
        description = '{} wants to delete conversation id={}.'.format(
            self.source_company, self.chat.id)

        notification = Notification.objects.create(
            user=self.user_profile.user,
            source=self.source_company,
            target=self.user_profile.company,
            description=description,
            link=magic_link
        )
        notification.save()



