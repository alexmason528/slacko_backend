import json
import logging

from django.conf import settings
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import get_authorization_header
from rest_framework_jwt.settings import api_settings
from constance import config
import django_filters

from userprofile.helpers import EmailThread
from company.models import Company, Notification
from .models import ChatFile, Message, Room, ConversationDeleteLink
from .serializers import ChatFileSerializer, ConverstationRequestSerializer
from .serializers import MessageSerializer, RoomSerializer

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

log = logging.getLogger(__name__)


@csrf_exempt
def new_room(request):
    """
    Randomly create a new room, and redirect to it.
    """
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        subject = requestBody['subject']
        target_id = requestBody['target_id']
        description = requestBody['description']
        files = requestBody['files']
        link = requestBody['link']
        url = requestBody['url']
        full_description = description
        if link:
            full_description += '\nLink:'+link
        if url:
            full_description += '\nUrl:'+url

        auth = get_authorization_header(request).split()
        payload = jwt_decode_handler(auth[1])
        current_user_id = payload.get('user_id')
        current_user = User.objects.get(pk=current_user_id)

        source_company = current_user.profile.company;
        target_company = Company.objects.get(pk=target_id)
        key = ''
        while True:
            key = get_random_string(64).lower()
            if not Room.objects.filter(key=key).exists():
                break
        newroom = Room.objects.create(source=source_company, name=subject, started=current_user,
                                      target=target_company, confirmed=False, key=key)
        newroom.members.add(current_user)
        newroom.members.add(target_company.owner)
        newroom.save()

        message = Message.objects.create(room=newroom, handle=current_user,
                                         message=full_description)
        message.save()

        for fid in files:
            try:
                ChatFile.objects.filter(id=fid).update(message=message)
            except:
                pass

        domain = config.FRONTEND_DOMAIN.rstrip('/')
        confirmation_link = domain + config.CONVERSATION_INVITE_LINK % key
        ctx_dict = {'confirmation_link': confirmation_link,
                    'source_company': source_company, 'description': subject + "\n" + full_description}
        message = render_to_string('email/conversation_invite_email.txt', ctx_dict)
        EmailThread("Slackco chat request", message, settings.DEFAULT_FROM_EMAIL, [target_company.owner.email]).start()
        # try:
            # for afile in request.FILES.getlist('attaches'):
                # mail.attach(afile.name, afile.read(), afile.content_type)
        # except:
            # pass
        # mail.send()
        notification_description = '{} would like to contact with your company'.format(source_company.name)
        notification = Notification.objects.create(
            user=target_company.owner,
            source=source_company,
            target=target_company,
            description=notification_description,
            link=confirmation_link
        )
        notification.save()
        return JsonResponse({'status': 'Success', 'message': "Conversation created and request sent", 'newroom_id': newroom.id})
    else:
        response = JsonResponse(
            {'status': 'Failed', 'message': "Request method should be POST"})
        return response


def get_current_user(requestData):
    auth = get_authorization_header(requestData).split()
    payload = jwt_decode_handler(auth[1])
    current_user_id = payload.get('user_id')
    current_user = User.objects.get(pk=current_user_id)
    return current_user


@csrf_exempt
def delete_conversation(request):
    try:
        current_user = get_current_user(request)

        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        room_id = requestBody['room_id']

        room = Room.objects.get(id=room_id)

        if room.source == current_user.profile.company:
            recipient = room.target.owner
        elif room.target != current_user.profile.company:
            recipient = room.source.owner
        else:
            return JsonResponse({'status': 'Failed', 'message': "Conversation does not exist"})

        request = ConversationDeleteLink.create(chat=room,
                                                user_profile=recipient.profile,
                                                source_company=current_user.profile.company )
        request.send_email()
        request.status = 1
        request.save()

        return JsonResponse({'status': 'Success', 'message': "Request sent"})
    except Room.DoesNotExist:
        return JsonResponse({'status': 'Failed', 'message': "Conversation does not exist"})


@csrf_exempt
def confirm_chat_delete(request, key):
    try:
        del_req = ConversationDeleteLink.objects.get(key=key)
        del_req.chat.delete()

        description = 'Chat with {} was deleted.'.format(
            del_req.user_profile.company)
        notification = Notification.objects.create(
            user=del_req.source_company.owner,
            source=del_req.user_profile.company,
            target=del_req.source_company,
            description=description
        )
        notification.save()
        return JsonResponse({'status': 'Success', 'message': "Conversation deleted"})
    except Room.DoesNotExist:
        return JsonResponse({'status': 'Failed', 'message': "Conversation does not exist"})


@csrf_exempt
def confirm_conversation(request, key):
    try:
        roomobj = Room.objects.get(key=key)
        current_user = get_current_user(request)
        roomobj.confirmed = True
        roomobj.members.add(current_user)
        roomobj.save()
        description = '{} Company accepted conversation request.'.format(
            roomobj.target.name)
        notification = Notification.objects.create(
            user=roomobj.source.owner,
            source=roomobj.target,
            target=roomobj.source,
            description=description
        )
        notification.save()
        return JsonResponse({'status': 'Success', 'message': "Conversation activated", 'chatroom_id': roomobj.id })
    except Room.DoesNotExist:
        return JsonResponse({'status': 'Failed', 'message': "Conversation does not exist"})


def chat_room(request, label):
    """
    Room view - show the room, with latest messages.

    The template for this view has the WebSocket business to send and stream
    messages, so see the template for where the magic happens.
    """
    # If the room with the given label doesn't exist, automatically create it
    # upon first visit (a la etherpad).
    room, created = Room.objects.get_or_create(label=label)

    # We want to show the last 50 messages, ordered most-recent-last
    messages = reversed(room.messages.order_by('-timestamp')[:50])

    return render(request, "chat/room.html", {
        'room': room,
        'messages': messages,
    })


@csrf_exempt
def del_member_to_room(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        room_id = requestBody['room_id']
        member_id = requestBody['member_id']
        member = User.objects.get(id=member_id)
        room = Room.objects.get(pk=room_id)
        room.members.remove(member)
        room.save()
        return JsonResponse({'status': 'Success', 'message': "Member was successfully deleted."})
    else:
        return JsonResponse({'status': 'Failed', 'message': "Request method should be POST"})


@csrf_exempt
def add_member_to_room(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        requestBody = json.loads(body_unicode)
        room_id = requestBody['room_id']
        members_id = requestBody['members_id']
        members = User.objects.filter(id__in=members_id)
        room = Room.objects.get(pk=room_id)
        room.members.set(members)
        room.save()
        return JsonResponse({'status': 'Success', 'message': "Members are successfully added."})
    else:
        return JsonResponse({'status': 'Failed', 'message': "Request method should be POST"})


class MessageFilter(filters.FilterSet):
    """docstring for MessageFilter"""
    timestamp_gte = django_filters.DateTimeFilter(
        name="timestamp", lookup_type='gte')
    timestamp_lte = django_filters.DateTimeFilter(
        name="timestamp", lookup_type='lte')

    class Meta:
        model = Message
        fields = ['room', 'timestamp_gte', 'timestamp_lte']


class ChatFileViewSet(viewsets.ModelViewSet):
    """docstring for ChatFileViewSet"""
    queryset = ChatFile.objects.all()
    serializer_class = ChatFileSerializer
    permission_classes = (IsAuthenticated,)


class RoomViewSet(viewsets.ModelViewSet):
    """docstring for RoomViewSet"""
    serializer_class = RoomSerializer
    # queryset = Room.objects.all()

    def get_queryset(self):
        return Room.objects.filter(members__id=self.request.user.id)


class MessageViewSet(viewsets.ModelViewSet):
    """docstring for MessageViewSet"""
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    filter_class = MessageFilter
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('room', 'timestamp', )


class ConversationRequestViewSet(viewsets.ModelViewSet):
    """docstring for MessageViewSet"""
    serializer_class = ConverstationRequestSerializer
    queryset = Room.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('target',)
