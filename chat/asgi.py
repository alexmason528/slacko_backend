import os
import channels.asgi

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slackco_backend.settings")
print("asgi file")
channel_layer = channels.asgi.get_channel_layer()