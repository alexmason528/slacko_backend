from django.contrib import admin
from .models import Room, Message, ChatFile, ConversationDeleteLink
# Register your models here.


class RoomAdmin(admin.ModelAdmin):
    """docstring for RoomAdmin"""
    list_display = ('id', 'source', 'target', 'confirmed')

admin.site.register(Room, RoomAdmin)


class MessageAdmin(admin.ModelAdmin):
    """docstring for RoomAdmin"""
    list_display = ('room', 'handle', 'status')

admin.site.register(Message, MessageAdmin)


class ChatFileAdmin(admin.ModelAdmin):
    """docstring for RoomAdmin"""
    list_display = ('id', 'file', 'filename', 'message')
admin.site.register(ChatFile, ChatFileAdmin)


class ConversationDeleteLinkAdmin(admin.ModelAdmin):
    """docstring for RoomAdmin"""
    list_display = ('id', 'chat', 'user_profile', 'source_company', 'sent',)
admin.site.register(ConversationDeleteLink, ConversationDeleteLinkAdmin)
