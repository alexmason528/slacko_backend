from rest_framework import serializers

from .models import ChatFile, Message, Room
from userprofile.serializers import UserSerializer
from company.serializers import CompanyProfileSerializer, CompanySerializer


class ChatFileSerializer(serializers.ModelSerializer):
    """docstring for ChatFileSerializer"""
    class Meta:
        """docstring for Meta"""
        model = ChatFile
        fields = ('id', 'file', 'filename')


class MessageSerializer(serializers.ModelSerializer):
    """docstring for MessageSerializer"""
    handle = UserSerializer(many=False)
    files = ChatFileSerializer(many=True)

    class Meta:
        """docstring for Meta"""
        model = Message
        # fields = ('id', 'file')


class RoomSerializer(serializers.ModelSerializer):
    """docstring for RoomSerializer"""
    members = UserSerializer(many=True)
    source = CompanySerializer(many=False)
    target = CompanySerializer(many=False)
    last_message = serializers.SerializerMethodField()
    started = UserSerializer(many=False)
    messages = MessageSerializer(many=True)

    def get_last_message(self, obj):
        try:
            msg = obj.messages.last()
            serializer = MessageSerializer(msg)
            return serializer.data
        except Exception:
            return None

    class Meta:
        """docstring for Meta"""
        model = Room
        fields = ('id', 'confirmed', 'key', 'members', 'started',
                  'messages', 'last_message', 'name', 'source', 'target')


class ConverstationRequestSerializer(serializers.ModelSerializer):
    """docstring for RoomSerializer"""
    source = CompanyProfileSerializer(many=False)
    target = CompanyProfileSerializer(many=False)

    class Meta:
        """docstring for Meta"""
        model = Room
        fields = ('id', 'confirmed', 'key', 'name', 'source', 'target')
