from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^new_room/$', views.new_room, name='new_room'),
    url(r'^add_member/$', views.add_member_to_room, name='add_member'),
    url(r'^del_member/$', views.del_member_to_room, name='del_member'),
    url(r'^delete/$', views.delete_conversation, name='delete_chat'),
    url(r'^confirm_delete/(?P<key>\w+)/$', views.confirm_chat_delete,
        name='confirm_delete_conversation'),
    url(r'^confirm/(?P<key>\w+)/$', views.confirm_conversation,
        name='confirm_conversation'),
]
