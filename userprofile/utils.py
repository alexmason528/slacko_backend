
def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'email': user['email'],
        'company': user['company']
    }
