<html>
<body>
	<p>Hello <b>{{ source_company }}</b> would like to contact with your company.
	</p>
	<br/>

	<p><a href="{{ confirmation_link }}">Accept</a></p><br/>
	<p>{{ description }}</p>
	<p>Please feel free to reach out to us any questions you may have by replying to this email.</p><br/>
</body>
</html>
